# Patches
* alpha (transparency to the dwm bar)
* alwaysontop (pin at top)
* combo (see multiple tags at once)
* fullgaps (useless gaps)
* hide_vacant_tags (hide tags without windows)
* moveresize (move/resize windows with arrows)
* noborder (noborder for an individual window)
* pertag (individual layout per tag 1|2|3|4...)
* resizecorners (resize windows at any corner)
* bottomstack (layout)
* centeredmaster (layout)
* fibonacci (layout)
* FLOATINGRULES
* SYSTRAY

# Combo fix
You need to changes two lines in `config.h` to the make the combo patch work.
```
/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      comboview,      {.ui = 1 << TAG} }, \   /* <-- Fix */
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      combotag,       {.ui = 1 << TAG} }, \   /* <-- Fix */
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },`
```

# Combo + pertag hack
`dwm.c` needs to be added one line to make both combo and pertag patches work at the same time.
```
void
comboview(const Arg *arg) {
	unsigned newtags = arg->ui & TAGMASK;
	if (combo) {
		selmon->tagset[selmon->seltags] |= newtags;
	} else {
        view(arg);   /* <-- Fix */
		selmon->seltags ^= 1;	/*toggle tagset*/
		combo = 1;
		if (newtags)
			selmon->tagset[selmon->seltags] = newtags;
	}
	focus(NULL);
	arrange(selmon);
}
```
# Discord setup
* dwm
fakefullscreen patch + ```firefox-developer-edition --no-remote -P discord --kiosk 'https://discord.com/login'```
* bspwmp
`ignore_ewmh_fullscreen` to all + `full-screen-api.ignore-widgets` to true on Firefox `about:config`
